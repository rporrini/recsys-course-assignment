set xlabel "Question"
set xrange [0:16]
set xtics ("Q1" 1, "Q2" 2, "Q3" 3, "Q4" 4, "Q5" 5, "Q6" 6, "Q7" 7, "Q8" 8, "Q9" 9, "Q10" 10, "Q11" 11, "Q12" 12, "Q13" 13, "Q14" 14, "Q15" 15) rotate by -30
set xtic scale 0

set ylabel "Mean Response (5-pt Likert scale)" rotate by 90
set yrange [1:5]
set ytics 1

set style data boxes
set key out top center

set term pdfcairo enhanced linewidth 2.2 font "Times New Roman,14"

plot "results.data" using ($1):2:(0.3) ls 1 fs solid 0.8 title "NETT-RS", \
"results.data" using ($1+0.30):3:(0.3) ls 2 fs solid 0.8 title "NETT-RS-b", \
"" using 1:2:2 with labels font ",10" offset -0.7,0.35 notitle, \
"" using 1:3:3 with labels font ",10" offset 1,0.4 notitle;

