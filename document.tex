\documentclass{llncs}
\usepackage[a4paper,plainpages=true,pdfpagelabels]{hyperref}
\usepackage[english]{babel}
\usepackage{graphicx}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{tabularx,booktabs}
\usepackage{multirow}

\begin{document}

\title{\textbf{Evaluation of Requirements Collection Strategies for a
Constraint-based Recommender System} \\
\small{Assignment for the course: \emph{Recommender Systems}}}
\author{Francesco Epifania \and Riccardo Porrini}
\institute{DISCo, University of Milano-Bicocca \\ Viale Sarca, 336/14, 20126,
Milan, Italy \\
		   \email{[francesco.epifania, riccardo.porrini]@disco.unimib.it}}

\maketitle

\begin{abstract}

The NETT Recommender System (NETT-RS) is a constraint-based recommender system
that recommends learning resources to teachers who want to design a course.
As for many state-of-the-art constraint-based recommender systems, the NETT-RS
bases its recommendation process on the collection of requirements to which
items must adhere in order to be recommended. In this report we study the
effects of two different requirement collection strategies on the perceived
overall recommendation quality of the NETT-RS. In the first strategy users are
not allowed to refine and change the requirements once chosen, while in the
second strategy the system allows the users to modify the requirements (we refer
to this strategy as \emph{backtracking}). We run the study following the well
established ResQue methodology for user-centric evaluation of RS. Our
experimental results indicate that backtracking has a strong positive impact on
the perceived recommendation quality of the NETT-RS.

\end{abstract}

\section{Introduction}

Recommender Systems (RSs) are information filtering algorithms that generate
meaningful recommendations to a set of users over a collection of items that
might be of their interest~\cite{rs-introduction}. In its basic incarnation, a
RS takes in input a user \emph{profile} and possibly some situational context
and computes a ranking over a collection of recommendable
items~\cite{AdomaviciusT05}. The user profile can possibly include explicit
information, such as feedback or ratings of items and/or implicit information,
such as items visited and time spent on them. RSs leverage this information to
predict the relevance score for a given, typically unseen, item.

RS have been adopted in many disparate fields, ranging from movies, music,
books, to financial services and live insurances~\cite{rs-introduction}. In the
e-Learning context, the NETT Recommender System (NETT-RS)~\cite{NETT-RS} is a RS
that recommends \emph{learning resources} (e.g., slides, tutorials, papers etc.)
to teachers that want to design a course. The NETT-RS is a component of the NETT
platform\footnote{\url{http://siren.laren.di.unimi.it/nett/mnett/}}, one of the
main outcome of the NETT European
project\footnote{\url{http://www.nett-project.eu/}}.
The NETT project aims at gathering a networked social community of teachers to
improve the entrepreneurship teaching in the European educational
system~\cite{nett}. Among the other things, the platform allows teachers to
design courses.

The NETT-RS supports the teachers in the design of courses by recommending
adequate and high quality learning resources (\emph{resources}, for brevity).
In order to finalize the design, teachers go through three sequential steps:
they specify (1) a set of \emph{rules} and (2) \emph{keywords} for a course
(e.g., required skill = statistics) and (3) the system recommends a set of
resources such that they fit the rules and the keywords specified by the teacher
(e.g., no differential calculus for a basic math course) and have an high
rating.

The characteristics of a NETT-RS closely match the ones proper of
\emph{constraint-based} RS~\cite{rs-handbook}, as the teacher specifies a set of
requirements (in the form of rules and keywords) to which resources must adhere
in order to be recommended. The multi-phased process allows the teacher to
incrementally explore the resource space in order to find the most suitable ones
for her/his course, in the vain of \emph{conversational}
RS~\cite{PuFCZV11,ChenP12}. However, this interaction with the user required by
the NETT-RS entails several challenges. The teacher must be put within an
interactive loop with the system, with the possibility to revise the rules and
keywords previously specified. We refer to this feature as \emph{backtracking}.

In this report, we study the effect of the backtracking feature on the NETT-RS.
We argue that providing a backtracking feature to the NETT-RS strongly
influences the perceived recommendation quality. In order to answer this
research question, we set up a \emph{user-centric} evaluation of the NETT-RS
following the ResQue methodology~\cite{resque}. We compare two versions of the
NETT-RS (with and without backtracking) over many different user-centric quality
dimensions. Evidence gathered from this study substantiates our intuitions:
the presence of backtracking has a strong impact on many different quality
measures, such as \emph{control}, \emph{perceived ease of use} and \emph{overall
satisfaction}.

The reminder of the report is organized as follows. In Section~\ref{sec:nett-rs}
we sketch the main components of the NETT-RS. In Section~\ref{sec:evaluation} we
describe the user study that we conducted and discuss the results. We compare
the NETT-RS with related work in Section~\ref{sec:related-work} and end the
report with conclusions and highlight future work in Section~\ref{sec:future}.

\section{The NETT Recommender System}\label{sec:nett-rs}

The NETT-RS recommendation process consists of three sequential steps:
\emph{rule induction}, \emph{keyword extraction} and \emph{resource selection}.
In the rest of the Section we describe how items (i.e., learning resources) are
represented within the NETT-RS, along with a sketch of the three phases. We also
highlight one of the main issues that underlies this multi-step process:
the need for backtracking.

\subsection{Learning Resources}\label{sec:resources}

\begin{table}[t]
\caption{An excerpt of metadata that characterize a resource.}
\label{table:metadata}
\scriptsize
\begin{tabularx}{\textwidth}{ l l X }
	\toprule
	\midrule
	\textbf{Metadata} & \textbf{Type} & \textbf{Values}\\
	\midrule
	\textit{Learning Resource Type} & qualitative & Diagram, Figure, Graph, Index,
	Slides, Table, Narrative Text, Lecture, Exercise, Simulation, Questionnaire, Exam, Experiment, Problem Statement, Self Assessment\\
	\midrule
	\textit{Format} & qualitative & Video, Images, Slide, Text, Audio\\
	\midrule
	\textit{Language} & qualitative & English, Italian, Bulgarian, Turkish\\
	\midrule
	\textit{Keywords} & qualitative & entrepreneurship, negotiation, \ldots\\
	\midrule
	\textit{Typical Learning Time} & quantitative & 30 minutes, 60 minutes, 90
	minutes, +120 minutes\\
	\midrule
	\bottomrule
\end{tabularx}
\end{table}

Resources are described using a set of \emph{metadata} that adhere to the
Learning Object Metadata
standard\footnote{\url{https://standards.ieee.org/findstds/standard/1484.12.1-2002.html}}
(LOM). These metadata characterize resources in terms of, for example,
\emph{format} (e.g., text, slide etc.) or \emph{language} (e.g., Italian,
English etc.). More formally, resources are characterized by a fixed set of $n$
metadata $\mu_1 \ldots \mu_n$, which can be \emph{qualitative} (nominal/ordinal)
or \emph{quantitative} (continuous/discrete), where the latter are suitably
normalized in [0, 1]. Table~\ref{table:metadata} presents some example metadata
used within the NETT-RS. The resources are also characterized by a particular
metadata: the \emph{keywords}. The keywords ideally describe the topics that a
resource is about. Finally, each resource has a textual content $\pi$ (e.g., the
text extracted from a slide). Each resource is affected by a rating $\rho$,
typically normalized in $[0, 1]$.

\subsection{Rule Induction}\label{sec:rules}

As a first step, the teacher is asked to select a set of constraints (i.e.,
\emph{rules}) over the learning metadata. Those rules are computed automatically
by the system leveraging a well known rule induction algorithm, as explained
later on in this Section. An example of rules selection is depicted in
Figure~\ref{fig:rules}. Rules, which should in principle accurately describe the
resources available in the system, are encoded as Horn clauses made of some
antecedents and one consequent. The consequent is fixed: ``$\pi$ is good''
(i.e., the content of a resource is good). The antecedents are Boolean
conditions $c_j$ (true/false) concerning sentences of two kinds: (1)
``$\mu_i~\frac{<}{>}~\theta$'', where $\theta$ stands for any symbolic (for
nominal metadata) or numeric constant (for quantitative variables) and (2)
``$\mu_i \in A$'', with $A$ a suitable set of constants associated with
qualitative metadata. A rule is hence formally defined as
$c_1,~\ldots,~c_k~\rightarrow~\pi~\text{is good}$.

In order to infer the rules, the NETT-RS applies RIPPERk, a variant of the
Incremental Reduced Error Pruning (IREP) proposed by Cohen~\cite{rule-induction}
and in particular its Java version JRip available in the WEKA
environment~\cite{weka}. The rules are presented to the teacher who decides
which rules most accurately characterize the material he/she is looking for. By
selecting such rules, the teacher narrows the set of resources to be potentially
recommended down, as the number of resources available in the NETT platform can
potentially be very high.

\begin{figure}[t]
	\begin{minipage}[b]{.5\textwidth}
	\centering
	\includegraphics[width=\linewidth]{images/rules.png}
	\caption{The rule selection step.}
	\label{fig:rules}
	\end{minipage}
	\hfill
	\begin{minipage}[b]{.5\textwidth}
	\centering
	\includegraphics[width=\linewidth]{images/keywords.png}
	\caption{The keywords selection step.}
	\label{fig:keywords}
	\end{minipage}
\end{figure}

\subsection{Keywords Extraction}\label{sec:keywords}

As a second step, the system presents the teacher with a subset of the keywords
extracted from the metadata of resources that satisfy the rules selected during
the rule induction phase. An example of keywords is depicted in
Figure~\ref{fig:keywords}. In fact, even after applying the filtering capability
provided by the selected rules, the number of resources that are to be suggested
can still be very high. Thus, a meaningful subset of the keywords is presented
to the teacher. The NETT-RS looks for the best subset of keywords in terms of
the ones providing the highest entropy partition of the resource set selected by
the rules. With this strategy, the number of selected resources is guaranteed to
reduce uniformly at an exponential rate for whatever keyword subset chosen by
the teacher.

\subsection{Resource Selection}

\begin{figure}[t]
	\centering
	\includegraphics[width=.6\textwidth]{images/resources.png}
	\caption{The resource selection step.}
	\label{fig:resources}
\end{figure}

As the final step, the NETT-RS recommends a set the resources such that: (1)
they satisfy the rules and (2) they are annotated with the selected keywords.
The teacher then finalizes the design of the course by selecting the resources
considered suitable. An example of suggested resources is depicted in
Figure~\ref{fig:resources}.

\subsection{The Backtracking Feature}\label{sec:why}
The NETT-RS requires the teacher to go through all the three steps described
above in order to finalize the design of a course. During each step the system
provides the teacher with a set of automatically selected items, being them
rules, keywords or resources, respectively. The strong implication in such a
process is that the choices made by the teacher in one phase can potentially
influence the result of the subsequent phases. For this reason we argue that
allowing the teacher to go back and forth the phases and possibly revising the
selections has a strong impact on the perceived quality of the resource
suggestion in the final step. The need of such a \emph{backtracking} feature was
furthermore observed by alpha testers of the NETT-RS, which initially did not
provide such feature.

\section{Evaluating the Backtracking Feature}\label{sec:evaluation}

From the user interaction point of view we argue that the backtracking feature
has an high impact on the overall perceived quality of the NETT-RS. We
substantiate this claim with empirical evidence gathered from a user-centric
evaluation of the NETT-RS. The remainder of this Section describes the conducted
experiment, starting from the research question and hypotheses, the experimental
setting, and ending with the discussion of the experimental results.

\subsection{Research Question and Hypotheses}

Our research question is rather simple and pragmatic:
\begin{center}
\fbox{
	\vbox{
	\hsize=\linewidth\noindent
	\centering
    Does providing a backtracking feature to teachers affect the perceived
    quality of the recommendation of the NETT System?
}}
\end{center}
In order to provide an answer to this research question, we evaluate the NETT-RS
and formulate the two following hypotheses:
\begin{description}
\item[H1:] the possibility to revise the choices made during the course design
process increases the \emph{perceived user control} over the NETT-RS.
\item[H2:] the possibility to revise the choices made during the course design
process increases the \emph{perceived overall quality} of the NETT-RS.
\end{description}
The hypothesis \textbf{H1} focuses on a specific quality of
the NETT-RS (i.e., the user control over the recommendation process), which is
only one of the possible dimensions that contribute to the \emph{perceived
overall quality} of system (\textbf{H2}).

\subsection{Experimental Design}

Two versions of the NETT-RS were evaluated: the first one without the
backtracking feature enabled (i.e., NETT-RS) and the second one with
backtracking (i.e., NETT-RS-b). So as for testing our hypotheses, we adopted the
ResQue methodology~\cite{resque}, being it a well established technique for the
user-centric evaluation of RSs. We selected 40 participants, mainly university
professors, and asked them to design a course on \emph{Statistics and
Probability}, choosing from 1170 different learning resources. We selected such
resources from the MIT OpenCourseWare
website\footnote{\url{http://ocw.mit.edu/index.htm}}. The participants were
equally partitioned into two \emph{disjoint} subsets (20 + 20). Participants
from the first subset were asked to design a course using NETT-RS, while
participants from the second subset used NETT-RS-b. Finally, participants were
presented with a questionnaire.

\begin{table}[t] 
\centering 
\scriptsize
\caption{The adapted version of the ResQue questionnaire used in our study.}
\begin{tabularx}{\textwidth}{l l X}
	\toprule
	\midrule
	& \textbf{Quality} & \textbf{Question} \\
	\midrule
	\textit{Q1} & \textit{recommendation accuracy} & The teaching material
	recommended to me match my interests \\\midrule
	\textit{Q2} & \textit{recommendation novelty} & The recommender
	system helped me discover new teaching material \\\midrule
	\textit{Q3} & \textit{recommendation diversity} & The items
	recommended to me show a great variety of options \\\midrule
	\textit{Q4} & \textit{interface adequacy} & The layout and labels of
	the recommender interface are adequate \\\midrule
	\textit{Q5} & \textit{explanation} & The recommender explains why the
	single teaching materials are recommended to me \\\midrule
	\textit{Q6} & \textit{information sufficiency} & The information
	provided for the recommended teaching material is sufficient for me to take
	a decision\\\midrule
	\textit{Q7} & \textit{interaction adequacy} & I found it easy to tell
	the system what I like/dislike \\\midrule
	\textit{Q8} & \textit{perceived ease of use} & I became familiar with
	the recommender system very quickly \\\midrule
	\textit{Q9} & \textit{control} & I feel in control of modifying
	my requests \\\midrule
	\textit{Q10} & \textit{transparency} & I understood why the learning material
	was recommended to me \\\midrule
	\textit{Q11} & \textit{perceived usefulness} & The recommender helped
	me find the ideal learning material \\\midrule
	\textit{Q12} & \textit{overall satisfaction} & Overall, I am satisfied
	with the recommender \\\midrule
	\textit{Q13} & \textit{confidence and trust} & The recommender can be
	trusted \\\midrule
	\textit{Q14} & \textit{use intentions} & I will use this recommender
	again \\\midrule
	\textit{Q15} & \textit{purchase intention} & I would adopt the learning
	materials recommended, given the opportunity \\\midrule
	\bottomrule
\end{tabularx}
\label{table:resque-questions}
\end{table}

\subsection{The (Adapted) ResQue Questionnaire}

The ResQue questionnaire~\cite{resque} defines a wide set of user-centric
quality metrics to evaluate the perceived qualities of RSs and
to predict users' behavioral intentions as a result of these qualities. The
original version of the questionnaire included 43 questions, evaluating 15
different qualities, such as \emph{recommendation accuracy} or \emph{control}.
Participants' responses to each question are characterized using a 5-point
Likert scale from \emph{strongly disagree} (1) to \emph{strongly agree} (5). 

Two versions of the questionnaire have been proposed~\cite{resque}: a longer
version (43 questions) and a shorter version (15 questions). In our study we
adopted the short version, as we aim at reducing the cognitive load required to
participants. A modified version of the questionnaire, tailored for a system
that recommends learning resources, was presented to the participants.
In fact, the ResQue questionnaire has been designed for the evaluation of a
``generic'' RS. Table~\ref{table:resque-questions} contains the adapted version
of the questionnaire.

\subsection{Experimental Results and Discussion}~\label{sec:experiments}

Table~\ref{table:results} reports the mean grades for all the questionnaire
questions. We report a Cronbach's $\alpha$~\cite{cronbach} equal to
\textbf{0.919} and \textbf{0.887} for grades given by participants who
evaluated the NETT-RS and the NETT-RS-b, respectively. Thus, we consider the
participants to the study to be reliable. NETT-RS-b achieves the most noticeable
result on the \emph{control} quality (\emph{Q9}): the presence of the
backtracking lifts the mean judgment up from \textbf{1.30} to \textbf{4.45}
(63\% of improvement). The difference is significant with a $p$-value $<
0.0001$, providing strong experimental evidence for the hypothesis \textbf{H1}:
the possibility to revise the choices made during the course design process
increases the \emph{perceived user control} over the NETT-RS. Results confirm
our expectations from Section~\ref{sec:why}.

As far as the overall quality is concerned (hypothesis \textbf{H2}) we observe
strong significant improvements ($p < 0.0001$) in the \emph{perceived ease of
use}, \emph{perceived usefulness}, \emph{overall satisfaction}, \emph{confidence
and trust}, \emph{use intentions} and \emph{purchase intention} qualities. This
evidence allows us to correlate the presence of the backtracking feature with an
higher perceived overall quality of the NETT-RS in terms of usability,
usefulness and user satisfaction. Again, we consider these experimental results
as evidence supporting our hypothesis: the possibility to revise the choices
made during the course design process increases the \emph{perceived overall
quality} of the RS.

The presence of the backtracking feature does not lead to a significant
improvement of the \emph{recommendation accuracy}. However, we observe
significant improvements ($p \approx 0.012$ and $p \approx 0.002$) on
\emph{recommendation novelty} and \emph{recommendation diversity}. Our
interpretation is that allowing the users to go back and forth the steps allows
them to better explore the resource space, thus leading to novel and diverse
recommendations. We believe this to be a crucial feature within the
application domain of the NETT-RS.

Finally, we observe that the presence of the backtracking feature has no
significant impact on the \emph{interface adequacy}, \emph{explanation} and
\emph{transparency} qualities. We furthermore observe that participants assigned
a relatively low grade, especially for the \emph{interface adequacy}. Such
results may come from the difficulty in understanding the meaning of rules
(Section~\ref{sec:rules}) presented by the NETT-RS. We consider it as a stimulus
for a future improvement of the system.

\begin{table}[t] \centering \caption{Mean grades to questionnaire's questions.
$p$-values are computed by means of a two-tailed t-test. Statistically
significant improvements are marked in bold.} \scriptsize
\begin{tabularx}{\textwidth}{@{}c c *3{>{\centering\arraybackslash}X}@{}}
	\toprule
	\midrule
	\textbf{Question} & \textbf{Quality} & \textbf{NETT-RS} & \textbf{NETT-RS-b} & $p$-value\\
	\midrule
	\textit{Q1} & \textit{recommendation accuracy} & 3.80 & 3.95 & 0.481\\
	\textit{Q2} & \textit{recommendation novelty} & 3.50 & \textbf{4.05} & 0.012 \\
	\textit{Q3} & \textit{recommendation diversity} & 3.50 & \textbf{4.10} & 0.002
	\\
	\textit{Q4} & \textit{interface adequacy} & 2.90 & 3.30 & 0.088 \\
	\textit{Q5} & \textit{explanation} & 3.40 & 3.60 & 0.162\\
	\textit{Q6} & \textit{information sufficiency} & 3.35 & \textbf{4.25} & $<$
	0.0006\\
	\textit{Q7} & \textit{interaction adequacy} & 3.10 & \textbf{3.60} & $<$
	0.002\\
	\textit{Q8} & \textit{perceived ease of use} & 3.45 & \textbf{4.60} & $<$
	0.0001\\
	\textit{Q9} & \textit{control} & 1.30 & \textbf{4.45} & $<$ 0.0001\\
	\textit{Q10} & \textit{transparency} &  3.45 & 3.75 & 0.110\\
	\textit{Q11} & \textit{perceived usefulness} & 3.00 & \textbf{4.00} & $<$
	0.0004\\
	\textit{Q12} & \textit{overall satisfaction} & 2.80 & \textbf{3.90} & $<$
	0.0001\\
	\textit{Q13} & \textit{confidence and trust} & 3.15 & \textbf{3.80} & $<$
	0.001\\
	\textit{Q14} & \textit{use intentions} & 2.70 & \textbf{3.70} & $<$ 0.0001\\
	\textit{Q15} & \textit{purchase intention} & 3.30 & \textbf{4.10} & $<$ 0.0001\\
	\midrule
	\bottomrule
\end{tabularx}
\label{table:results}
\end{table}

\section{Related Work}\label{sec:related-work}

A widely accepted classification of RSs divides them into four main
families~\cite{rs-introduction}: \emph{content-based} (CB), \emph{collaborative
filtering} (CF), \emph{knowledge-based} (KB) and \emph{hybrid}. The basic idea
behind CB RSs is to recommend items that are similar to those that the user
liked in the past (see e.g., \cite{BalabanovicS97,PazzaniB97,Mooney2000}). CF RSs
recommend items based on the past ratings of all users collectively (see e.g.,
\cite{Resnick1994,Sarwar2001,LemireM05}). KB RSs suggest items based on
inferences about users' needs: domain knowledge is modeled and leveraged during
the recommendation process (see
e.g.,~\cite{Burke2000,Felfernig2008,FelfernigK05}). Hybrid RSs usually combine
two or more recommendation strategies together in order to leverage the
strengths of them in a principled way (see
e.g.,~\cite{deCampos2010,Shinde2012,RenHGXW08}).

The NETT-RS falls into the KB RSs family, and more precisely into the
\emph{constraint-based} category. For a more exhaustive and complete description
of constraint-based RSs we point the reader to~\cite{rs-handbook}. The typical
features of such RSs are: (1) the presence of a \emph{knowledge base} which
models both the items to be recommended and the explicit rules about how to
relate user requirements to items, (2) the collection of user requirements, (3)
the repairment of possibly inconsistent requirements, and (4) the explanation of
recommendation results.

We recall from Section~\ref{sec:resources} that learning resources in the
NETT-RS are characterized by \emph{metadata}. This characterization provides the
basic building block for the construction of a knowledge base (e.g., using
Semantic Web practices tailored for the education domain~\cite{Dietze2013}).
As for the collection of user requirements, the NETT-RS collects them during the
rule and keywords selection phases, described in Sections~\ref{sec:rules}
and~\ref{sec:keywords}, respectively. The NETT-RS does not provide any kind of
repairment for inconsistent requirements (i.e., rules and keywords), in contrast
with most state-of-the-art constraint-based
RSs~\cite{FelfernigK05,Felfernig2008,FelfernigFISTJ09}. However, we notice that
the interaction that the NETT-RS requires to the teachers is different:
rules and keywords are not directly specified. Instead, teachers specify the
requirements by choosing from a suggested set of available rules and keywords,
ensuring the specification of consistent requirements only. Finally, the NETT-RS
currently does not provide any explanation of recommendation results. However,
as pointed out by our experiments in Section~\ref{sec:experiments}, the system
would benefit from the application of such explanation
techniques~\cite{FriedrichZ11}.

In KB RSs literature, special attention has been devoted to requirements
collection, being it a mandatory prerequisite for recommendations to be
made~\cite{rs-handbook}. Requirements can be collected using different
strategies, each one leading to different interaction mechanisms with the user.
Such mechanisms can be relatively simple as \emph{static fill-out} forms filled
each time a user accesses the RS, but also more sophisticated
like the \emph{interactive conversational dialogs}, where the user specifies and
refines the requirements incrementally by interacting with the
system~\cite{PuFCZV11,ChenP12}. The backtracking feature added to the NETT-RS
goes exactly towards this direction.

\section{Conclusion and Future Work}\label{sec:future}

We conducted a user-centric evaluation of the constraint-based NETT-RS, a RS
that recommends resources to teachers who want to design a course. Our goal was
to study the effect on the overall perceived recommendation quality of a
\emph{backtracking} feature, that is to give the possibility to teachers to
revise the constraints (i.e., rules and keywords) over the resources specified
within the recommendation process. Our study reveals a strong correlation
between the presence of the backtracking feature and an higher perceived
quality.

We foresee at least two main future lines of work. From the experimental point
of view, we would like to run the experiment on learning resources from
different domains and include more participants. From the point of view of the
NETT-RS itself, we plan to take advantage of the insights that we got from this
user study and include in the system also the explanation of the recommendation
results inspired by related work in this
area~\cite{FelfernigK05,Felfernig2008,FelfernigFISTJ09}.

\bibliographystyle{splncs03}
\bibliography{biblio}

\end{document}